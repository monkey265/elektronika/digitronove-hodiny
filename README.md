# Úvod


# BOM

| Jméno 	| Hodnota, typ       | Značka hotova 	   | Footprint hotov   |
|-------	|---------	         |---------------	   |-----------------  |
| R1  - R12 | 10k                |       [x]           |        [x]        |
| R19 - R30 | 10k                |       [x]           |        [x]        |
| R35       | 10k                |       [x]           |        [x]        |
| R13 - R18 | 1M                 |       [x]           |        [x]        |
| R31       | 47k                |       [x]           |        [x]        |
| R32       | 1                  |       [x]           |        [x]        |
| R33       | 1,2M               |       [x]           |        [x]        |
| R34       | 1k                 |       [x]           |        [x]        |
| R36       | 200k (250k) trimr  |       [x]           |        [x]        |
| R37       | Nutno specifikovat |       [x]           |        [x]        |
| C1, C2    | Dle mikroprocesoru |       [ ]           |        [ ]        |
| C3        | 1nF                |       [ ]           |        [ ]        |
| C4        | 100nF/250V, MKS    |       [ ]           |        [ ]        |
| C5        | 4,7uF/250V         |       [ ]           |        [ ]        |
| C6        | 220uF/16V          |       [ ]           |        [ ]        |
| C7, C8, C9| 100nF, keramický   |       [ ]           |        [ ]        |
| C10       | 22uF/10V           |       [ ]           |        [ ]        |
| L1        | 330uF, tlumivka    |       [ ]           |        [ ]        |
| D1, D2, D3| 1N4148             |       [ ]           |        [ ]        |
| D4        | MUR1100            |       [ ]           |        [ ]        |
| D5        | 1N4007             |       [ ]           |        [ ]        |
| D6, D7, D8| 1N5819             |       [ ]           |        [ ]        |
| T1 - T6   | MPSA92             |       [ ]           |        [ ]        |
| T7 - T12  | MPSA42             |       [ ]           |        [ ]        |
| T13       | IRF830             |       [ ]           |        [ ]        |
| U1        | Procesor           |       [x]           |        [x]        |
| U2        | 74141              |       [x]           |        [x]        |
| U3        | MC34063            |       [ ]           |        [ ]        |
| U4        | 7805               |       [ ]           |        [ ]        |
| XT1       | Nutno specifikovat |       [ ]           |        [ ]        |
| B1, B2, B3| Tlačítko           |       [ ]           |        [ ]        |
| SW1       | Spínač             |       [ ]           |        [ ]        |
| N1 - N6   | Digitrony          |       [x]           |        [x]        |


# Zdroje a použité open source 

- [Eagle-and-KiCAD-Nixie-Libs](https://github.com/judge2005/Eagle-and-KiCAD-Nixie-Libs?tab=readme-ov-file)